## * Header

## Imports csv files in given directory and returns the mean of the given
## pollutant from defined monitoring stations.

## * Function

pollutantmean <- function(directory, pollutant, id = 1 : 332) {
  ## Get list of files in directory
  files_list <- list.files(path = directory, pattern = "*.csv", full.names = TRUE)
  ## Combine all files in one list
  data <- lapply(files_list, read.csv)
  ## Merge data into one data frame
  data <- do.call(rbind, data)
  ## Select required data and calculate mean
  mean(data[data$ID %in% c(id),][,pollutant], na.rm = TRUE)
}
