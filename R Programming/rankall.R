library(dplyr)

rankall <- function(outcome = c("heart attack",
                                "heart failure",
                                "pneumonia"),
                    num = "best") {
  ## Check is outcome is a single element and a string
  if (length(outcome) != 1) {
    stop("outcome needs to be a single element")}
  if (is.character(outcome) != TRUE) {
    stop("outcome needs to be a string")}
  
  ## Read outcome data
  data <- read.csv("outcome-of-care-measures.csv", colClasses = "character")
  ## Assign relevant data to new data frame
  df <- data[c(2, 7, 11, 17, 23)]
  ## Replace "Not available" strings with NA
  df[df == "Not Available"] <- NA
  ## Make numeric
  df[, c(3:5)] <- sapply(df[, c(3:5)], as.numeric)
  ## Assign new/easier names
  names(df) <- c("name", "state", "heart attack", "heart failure", "pneumonia")

  ## Check that state and outcome are valid
  if (outcome %in% c("heart attack", "heart failure", "pneumonia")) {
    ## Return hospital name with the given rank 30-day death
    ## rate
    ## Translate "best" and "worst"
    if (num %in% c("best", "worst")) {
      rank_num  <- 1}
      else {
        rank_num <- num}

    ## Get candidates sorted by outcome value and ranked
    candidates <- df %>% group_by(state)
    ## Take care of "best" and "worst"
    if (num == "worst") {
      candidates <- candidates %>% arrange(desc(get(outcome)))}
    else {
        candidates <- candidates %>% arrange(get(outcome))}
    candidates <- candidates %>% mutate(rank = order(order(get(outcome))))
    
    ## Split into states
    candidates <- split(candidates, candidates$state)

    ## Get hospitals with required rank. Will return NA if it does not exists
    hospitals <- do.call(rbind, lapply(candidates, function(x) {x[rank_num, c(1)]}))

    ## Get states
    states <- do.call(rbind, lapply(candidates, function(x) {x[1, c(2)]}))
    
    ## Make a nice data frame
    cbind(setNames(data.frame(hospitals), c("hospital")),
          setNames(data.frame(states), c("state")))
    }
  else {
    stop("invalid outcome")
  }
}
