library(dplyr)

rankhospital <- function(state, 
                         outcome = c("heart attack",
                                     "heart failure",
                                     "pneumonia"),
                         num = "best") {
  ## Check if state is a string
  if (is.character(state) != TRUE) {
    stop("state needs to be a string")}
  ## Check is outcome is a single element and a string
  if (length(outcome) != 1) {
    stop("outcome needs to be a single element")}
  if (is.character(outcome) != TRUE) {
    stop("outcome needs to be a string")}
  
  ## Read outcome data
  data <- read.csv("outcome-of-care-measures.csv", colClasses = "character")
  ## Assign relevant data to new data frame
  df <- data[c(2, 7, 11, 17, 23)]
  ## Replace "Not available" strings with NA
  df[df == "Not Available"] <- NA
  ## Make numeric
  df[, c(3:5)] <- sapply(df[, c(3:5)], as.numeric)
  ## Assign new/easier names
  names(df) <- c("name", "state", "heart attack", "heart failure", "pneumonia")

  ## Check that state and outcome are valid
  if (state %in% df$state) {
    if (outcome %in% c("heart attack", "heart failure", "pneumonia")) {
      ## Return hospital name in that state with the given rank 30-day death
      ## rate
      ## Get candidates sorted by outcome value and their name
      candidates <- df %>% filter(state == !!state) %>% 
        arrange(get(outcome), name)
      ## Add rank and rate column
      ## Remove NAs
      candidates <- candidates %>% mutate(rate = get(outcome), 
                                          rank = seq(1, nrow(candidates))) %>%
        select(c(name, rate, rank)) %>% na.omit()
      ## Translate "best" and "worst"
      if (num == "best") {
        num  <- 1}
      else if (num == "worst") {
        num <- nrow(candidates)}
      else {
        num <- num}
      ## Return NA if num is larger than candidate list
      if (num > nrow(candidates)) {
        return(NA)}
      candidates %>% filter(rank == num)
    }
    else {
    stop("invalid state")
    }
  }
  else {
  stop("invalid outcome")
  }
}
