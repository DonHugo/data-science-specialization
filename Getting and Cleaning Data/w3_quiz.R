asc <- read.csv(file = "~/Programmieren/Data Science/Getting and Cleaning Data/getdata_data_ss06hid.csv", header = TRUE)
agricultureLogical <- asc$ACR == 3 & asc$AGS == 6
head(agricultureLogical)
which(agricultureLogical)

library(jpeg)

pic <- readJPEG(source = "~/Programmieren/Data Science/Getting and Cleaning Data/getdata_jeff.jpg", native = TRUE)
quantile(pic, probs = c(0.3, 0.8))

GDP <- read.csv(file = "~/Programmieren/Data Science/Getting and Cleaning Data/getdata_data_GDP.csv", skip = 5, nrows = 190, header = FALSE)
GDP <- GDP[c(1,2,4,5)]
## tail(GDP)
names(GDP)  <- c("CountryCode", "Ranking", "Long Name", "GDP")
EDU <- read.csv(file = "~/Programmieren/Data Science/Getting and Cleaning Data/getdata_data_EDSTATS_Country.csv")

library(plyr)
merged_data <- join(GDP, EDU, by = "CountryCode", type = "inner")
## merged_data <- merge(GDP, EDU, by.x = "CountryCode", by.y = "CountryCode", all = FALSE)
head(merged_data)
nrow(merged_data)
library(dplyr)
View(merged_data)
merged_data %>% dplyr::arrange(desc(Ranking)) %>% head(n = 15)

merged_data %>% group_by(Income.Group) %>% summarise(avg = mean(Ranking))

merged_data %>% group_by(G = cut(Ranking, breaks = quantile(Ranking, probs = seq(0, 1, by = 0.2))), Income.Group) %>% summarise(n())
