asc <- read.csv(file = "~/Programmieren/Data Science/Getting and Cleaning Data/getdata_data_ss06hid.csv", header = TRUEd)
df_names <- names(asc)
res <- strsplit(df_names, "wgtp")
res[123]

GDP <- read.csv(file = "~/Programmieren/Data Science/Getting and Cleaning Data/getdata_data_GDP.csv", skip = 5, nrows = 190, header = FALSE)
GDP <- GDP[c(1,2,4,5)]
## tail(GDP)
names(GDP)  <- c("CountryCode", "Ranking", "Long Name", "GDP")
GDP$GDP <- as.numeric(gsub(",", "", GDP$GDP))
mean(GDP$GDP)


GDP <- read.csv(file = "~/Programmieren/Data Science/Getting and Cleaning Data/getdata_data_GDP.csv", skip = 5, nrows = 190, header = FALSE)
GDP <- GDP[c(1,2,4,5)]
## tail(GDP)
names(GDP)  <- c("CountryCode", "Ranking", "Long Name", "GDP")
EDU <- read.csv(file = "~/Programmieren/Data Science/Getting and Cleaning Data/getdata_data_EDSTATS_Country.csv")

library(plyr)
merged_data <- join(GDP, EDU, by = "CountryCode", type = "inner")
## merged_data <- merge(GDP, EDU, by.x = "CountryCode", by.y = "CountryCode", all = FALSE)
grep("June", merged_data$Special.Notes)

library(quantmod)
amzn = getSymbols("AMZN",auto.assign=FALSE)
sampleTimes = index(amzn)

library(lubridate)
library(dplyr)

length(year(sampleTimes)[year(sampleTimes) == 2012 & wday(sampleTimes, label = TRUE) == "Mo"])
length(wday(sampleTimes, label = TRUE)[wday(sampleTimes, label = TRUE) == "Mo"])
